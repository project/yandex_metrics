<?php

/**
 * @file
 * Report callbacks for Yandex.Metrics Reports module.
 */

/**
 * The function generates content of search phrases table ordered by popularity.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 *
 * @return mixed
 *   Content table.
 */
function yandex_metrics_reports_search_phrases($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $report_phrases = yandex_metrics_reports_retreive_data('/stat/sources/phrases', $parameters);
  $phrases = json_decode($report_phrases->data);

  if (empty($phrases->data)) {
    return t('There is no information about search phrases for the selected date range.');
  }

  $phrases_totals_visits = $phrases->totals[0];

  $header = array(t('Visits (%)'), t('Phrase'));

  $data = array();

  $i = 1;
  foreach ($phrases->data as $value) {
    if ($i > YANDEX_METRICS_REPORTS_SEARCH_PHRASES_QUANTITY) {
      break;
    }
    $percentage = round(100 * $value->metrics[0] / $phrases_totals_visits, 2);
    $data[] = array($percentage, check_plain($value->dimensions[0]->name));
    $i++;
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $data,
    'caption' => t('Popular Search Phrases'),
  ));
}

/**
 * The function generates pie chart with traffic sources summary.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_sources_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $results = yandex_metrics_reports_retreive_data('/stat/sources/summary', $parameters);
  $summary = json_decode($results->data);
  if (empty($summary->data)) {
    return t('There is no information about traffic sources for the selected date range.');
  }

  $chart = array(
    'title' => t('Traffic Sources'),
    'type' => 'pie',
    'fields' => array(
      'sources' => array(
        'label' => t('Sources'),
        'enabled' => FALSE,
      ),
      'visits' => array(
        'label' => t('Visits'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'sources',
    ),
  );

  $sum = $summary->totals[0];

  $i = 1;
  foreach ($summary->data as $value) {
    $name = check_plain($value->dimensions[0]->name);
    $sources_leneged = $i . '. ' . $name . ' (' . round($value->metrics[0] * 100 / $sum) . '%' . ')';

    $chart['data'][] = array(
      'sources' => $sources_leneged,
      'visits' => (int) $value->metrics[0],
    );

    $i++;
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * Generates chart with information about page views, visitors and new visitors.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_visits_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  if (isset($date_range['group'])) {
    $parameters['group'] = $date_range['group'];
  }

  $results = yandex_metrics_reports_retreive_data('/stat/traffic/summary', $parameters);
  $visits = json_decode($results->data);
  if (empty($visits->data)) {
    return t('There is no information about page views and visitors for the selected date range.');
  }

  $chart = array(
    'title' => t('Page Views, Visitors, New Visitors'),
    'type' => 'line',
    'fields' => array(
      'dates' => array(
        'label' => t('Dates'),
        'enabled' => FALSE,
      ),
      'page_views' => array(
        'label' => t('Page Views'),
        'enabled' => TRUE,
      ),
      'visitors' => array(
        'label' => t('Visitors'),
        'enabled' => TRUE,
      ),
      'new_visitors' => array(
        'label' => t('New Visitors'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'dates',
    ),
  );

  // @todo Check: can we replace array_reverse with API sorting?
  foreach (array_reverse($visits->data) as $value) {
    $timestamp = strtotime(check_plain($value->dimensions[0]->name));

    $chart['data'][] = array(
      'dates' => date('d.m.y', $timestamp),
      'page_views' => (int) $value->metrics[0],
      'visitors' => (int) $value->metrics[1],
      'new_visitors' => (int) $value->metrics[2],
    );
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * The function generates the table of popular content.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_popular_content($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'limit' => YANDEX_METRICS_REPORTS_POPULAR_CONTENT_COUNT,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $report_content = yandex_metrics_reports_retreive_data('/stat/content/popular', $parameters);
  $content = json_decode($report_content->data);

  if (empty($content->data)) {
    return t('There is no information about popular content for the selected date range.');
  }

  $header = array(t('URL'), t('Page Views'));

  $data = array();

  $i = 1;
  foreach ($content->data as $value) {
    if ($i > YANDEX_METRICS_REPORTS_POPULAR_CONTENT_COUNT) {
      break;
    }
    $page_views = (int) $value->metrics[0];
    $data[] = array(
      l($value->dimensions[4]->name, $value->dimensions[4]->name, array(
        'attributes' => array('target' => '_blank'),
      )),
      $page_views,
    );
    $i++;
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $data,
    'caption' => t("Popular Content"),
  ));
}

/**
 * The function generates pie chart with geographical information on visitors.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_geo_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $results = yandex_metrics_reports_retreive_data('/stat/geo', $parameters);
  $geo = json_decode($results->data);
  if (empty($geo->data)) {
    return t('There is no information about geography of visits for the selected date range.');
  }

  $chart = array(
    'title' => t('Geography of Visits'),
    'type' => 'pie',
    'fields' => array(
      'country' => array(
        'label' => t('Country'),
        'enabled' => FALSE,
      ),
      'visits' => array(
        'label' => t('Visits'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'country',
    ),
  );

  $total_visits = $geo->totals[0];

  // Exclude unknown visits.
  foreach ($geo->data as $key => $value) {
    if (is_null($value->dimensions[2]->name)) {
      // @todo Add (int) for value?
      $total_visits -= $value->metrics[0];
      unset($geo->data[$key]);
      // @todo Do we need break for new API version?
      // break;
    }
  }

  $i = 1;
  $sum_visits = 0;
  foreach ($geo->data as $value) {

    $visits = (int) $value->metrics[0];

    if ($i > 10) {
      $others_visits = $total_visits - $sum_visits;

      $chart['data'][] = array(
        'country' => t('Others'),
        'visits' => $others_visits,
      );

      break;
    }

    $sum_visits += $visits;

    $name = check_plain($value->dimensions[2]->name);
    $chart['data'][] = array(
      'country' => $name,
      'visits' => $visits,
    );

    $i++;
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * The function generates pie chart with browser information on visitors.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_browser_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $results = yandex_metrics_reports_retreive_data('/stat/tech/browsers', $parameters);
  $browsers = json_decode($results->data);
  if (empty($browsers->data)) {
    return t('There is no information about visitors browsers for the selected date range.');
  }

  $chart = array(
    'title' => t('Browsers'),
    'type' => 'pie',
    'fields' => array(
      'country' => array(
        'label' => t('Browser'),
        'enabled' => FALSE,
      ),
      'visits' => array(
        'label' => t('Visits'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'browser',
    ),
  );

  $total_visits = $browsers->totals[0];

  // Exclude unknown visits.
  foreach ($browsers->data as $key => $value) {
    if (is_null($value->dimensions[1]->name)) {
      // @todo Add (int) for value?
      $total_visits -= $value->metrics[0];
      unset($browsers->data[$key]);
      // @todo Do we need break for new API version?
      // break;
    }
  }

  $i = 1;
  $sum_visits = 0;
  foreach ($browsers->data as $value) {

    $visits = (int) $value->metrics[0];

    if ($i > 10) {
      $others_visits = $total_visits - $sum_visits;

      $chart['data'][] = array(
        'browser' => t('Others'),
        'visits' => $others_visits,
      );

      break;
    }

    $sum_visits += $visits;

    $name = check_plain($value->dimensions[1]->name);
    $chart['data'][] = array(
      'browser' => $name,
      'visits' => $visits,
    );

    $i++;
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * The function generates pie chart with OS information on visitors.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_os_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $results = yandex_metrics_reports_retreive_data('/stat/tech/os', $parameters);
  $os = json_decode($results->data);
  if (empty($os->data)) {
    return t('There is no information about visitors operation systems for the selected date range.');
  }

  $chart = array(
    'title' => t('Operation systems'),
    'type' => 'pie',
    'fields' => array(
      'country' => array(
        'label' => t('OS'),
        'enabled' => FALSE,
      ),
      'visits' => array(
        'label' => t('Visits'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'os',
    ),
  );

  $total_visits = $os->totals[0];

  // Exclude unknown visits.
  foreach ($os->data as $key => $value) {
    if (is_null($value->dimensions[1]->name)) {
      // @todo Add (int) for value?
      $total_visits -= $value->metrics[0];
      unset($os->data[$key]);
      // @todo Do we need break for new API version?
      // break;
    }
  }

  $i = 1;
  $sum_visits = 0;
  foreach ($os->data as $value) {

    $visits = (int) $value->metrics[0];

    if ($i > 10) {
      $others_visits = $total_visits - $sum_visits;

      $chart['data'][] = array(
        'os' => t('Others'),
        'visits' => $others_visits,
      );

      break;
    }

    $sum_visits += $visits;

    $name = check_plain($value->dimensions[1]->name);
    $chart['data'][] = array(
      'os' => $name,
      'visits' => $visits,
    );

    $i++;
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * The function generates chart with information about hourly traffic.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_traffic_hourly_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  if (isset($date_range['group'])) {
    $parameters['group'] = $date_range['group'];
  }

  $results = yandex_metrics_reports_retreive_data('/stat/traffic/hourly', $parameters);
  $hourly_report = json_decode($results->data);
  if (empty($hourly_report->data)) {
    return t('There is no information about hourly traffic for the selected date range.');
  }

  $chart = array(
    'title' => t('Hourly Traffic'),
    'type' => 'column',
    'fields' => array(
      'hours' => array(
        'label' => t('Hours'),
        'enabled' => FALSE,
      ),
      'avg_visits' => array(
        'label' => t('Visits per hour'),
        'enabled' => TRUE,
      ),
      'denials' => array(
        'label' => t('Denials in % (visits with only one page view)'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'hours',
    ),
  );

  foreach ($hourly_report->data as $hour_data) {
    list($hour,) = explode(':', $hour_data->dimensions[0]->name);
    $chart['data'][] = array(
      'hours' => $hour,
      'avg_visits' => round($hour_data->metrics[0], 2),
      // @todo Check denials and remove below 2 lines:
      // Convert denials from percents.
      /*
        'denials' => round($hour_data->metrics[0] * $hour_data->metrics[1], 2),
      */
      'denials' => round($hour_data->metrics[1], 2),
    );
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * The function generates pie chart with demography information.
 *
 * @param string $counter_id
 *   Counter id.
 * @param string $filter
 *   Date range filter.
 */
function yandex_metrics_reports_gender_chart($counter_id, $filter) {

  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date'],
  );

  $results = yandex_metrics_reports_retreive_data('/stat/demography/structure', $parameters);
  $demography = json_decode($results->data);
  if (empty($demography->data)) {
    return t('There is no demography information for the selected date range.');
  }

  $chart = array(
    'title' => t('Demography of Visits'),
    'type' => 'pie',
    'fields' => array(
      'gender' => array(
        'label' => t('Gender'),
        'enabled' => FALSE,
      ),
      'visits' => array(
        'label' => t('Visits'),
        'enabled' => TRUE,
      ),
    ),
    'xAxis' => array(
      'labelField' => 'gender',
    ),
  );

  $data = $demography->data;

  // Add key for sorting stability.
  foreach ($data as $key => $value) {
    $data[$key]->key = $key;
  }

  // Sort data by gender.
  usort($data, '_yandex_metrics_reports_gender_sort');

  $sum = $demography->totals[0];
  $label_counter = 1;
  foreach ($data as $value) {
    if ($value->metrics[0] === 0) {
      continue;
    }

    $age = check_plain($value->dimensions[0]->name);
    $name_gender = check_plain($value->dimensions[1]->name);
    $gender_legend = $label_counter . '.' . $name_gender . '/' . $age . ' (' . round($value->metrics[0] * 100 / $sum) . '%' . ')';
    $chart['data'][] = array(
      'gender' => $gender_legend,
      'visits' => (int) $value->metrics[0],
    );

    $label_counter++;
  }

  return theme('visualization', array('options' => $chart));
}

/**
 * Sort callback to order data objects by gender DESC, then by key ASC.
 */
function _yandex_metrics_reports_gender_sort($a, $b) {
  if ($a->metrics[0] == $b->metrics[0]) {
    return ($a->key < $b->key) ? -1 : 1;
  }

  return ($a->metrics[0] > $b->metrics[0]) ? -1 : 1;
}
